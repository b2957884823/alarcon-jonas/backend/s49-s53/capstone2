// Mongoose Dependency
const mongoose = require('mongoose');

// Cart model

const cartSchema = new mongoose.Schema({
	userId: {
		type: String,
		required: [true, 'User Id is required.']
	},
	products: [
		{
			productId: {
				type: String,
				required: [true, 'Product Id is required.']
			},
			quantity: {
				type: Number,
				required: [true, 'Quantity is required.']
			}
		}
	],
	totalAmount: {
		type: Number,
		required: [true, 'Total amount is required.']
	},
	purchasedOn: {
		type: Date,
	default: new Date()
	}
});

module.exports = mongoose.model('Cart', cartSchema);