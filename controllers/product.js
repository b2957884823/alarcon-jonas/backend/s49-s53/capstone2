const Product = require("../models/Product");
const User = require("../models/User");




// "addproduct controller"

module.exports.addProduct = (req, res) => {

    let newProduct = new Product({
        name: req.body.name,
        description: req.body.description,
        price: req.body.price
    })

    
    return newProduct.save().then((product, error) => {

        if(error) {
            return res.send(false);

        } else {
            return res.send(true);
        }

    }).catch(err => res.send(err));
};




// Retrieve all product
/*
    Business Logic:
        1. Retrieve all the courses from the courss collection from the database.
*/


// We used the find( method of our Course model.
// Because our Course model is connected to our courses collection
// Course.find({}) is the same as db.courses.find({})
// empty {} will return all the documents from the courses collection.

module.exports.getAllProduct = (req, res) => {
    return Product.find({}).then(result => {
        // console.log(result)
        return res.send(result);
    })
    .catch(err => res.send(err))
};



// Retrieve All ACTIVE products
module.exports.getAllActive = (req, res) => {
    return Product.find({ isAvailable: true }).then(result => {
        // console.log(result)
        return res.send(result)
    })
    .catch(err => res.send(err))
};



// This function is for viewing single product
module.exports.getSingleProduct = (req, res) => {
    return Product.findById(req.params.productId).then(result => {
        // console.log(result)
        return res.send(result);
    })
    .catch(err => res.send(err))
};




// updating the product by admin only
module.exports.updateProduct = (req, res) => {

    // specify the fields and properties of the document to be updated
    let updatedProduct = {
        name: req.body.name,
        description: req.body.description,
        price: req.body.price
    }

    // Syntax:
        // findByIdAndUpdate(documentId, updatesToBeApplied)
    return Product.findByIdAndUpdate(req.params.productId, updatedProduct)
        .then((product, error) => {

        console.log(product)
        // Course is not updated
        if(error) {
            return res.send(false);
        // Course updated successfully
        } else {
            return res.send(true);
        }

    }).catch(err => res.send(err))
};


// [archiveProduct]
module.exports.archiveProduct = (req, res) => {

    let archivedProduct = {
        isAvailable: false
    }
    return Product.findByIdAndUpdate(req.params.productId, archivedProduct)
    .then((product, error) => {

        if(error) {
            return res.send(false);
        } else {
            return res.send(true);
        }

    }).catch(err => res.send(err))
};



// [activateProduct]
module.exports.activateProduct = (req, res) => {

    let activatedProduct = {
        isAvailable: true
    }

    return Product.findByIdAndUpdate(req.params.productId, activatedProduct)
    .then((product, error) => {

        if(error) {
            return res.send(false);
        } else {
            return res.send(true);
        }

    }).catch(err => res.send(err))
}

module.exports.searchProductsByName = async (req, res) => {
  try {
    const { productName } = req.body;

    const products = await Product.find({
      name: { $regex: productName, $options: 'i' }
    });

    res.json(products);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
};


