// Dependencies and Modules
const Product = require("../models/Product");
const User = require("../models/User");



const bcrypt = require('bcrypt');

const auth = require('../auth');


module.exports.checkEmailExists = (reqBody) => {
    return User.find({email: reqBody.email}).then(result => {
        // The "find" method returns a record if a match is found.
        if(result.length > 0) {
            return true;
        } else {
            return false;
        }
    })
};


module.exports.registerUser = (reqBody) => {
 // Create a variable newUser and instantiates a new User object using the mongoose model. 
    // Uses the information from the request body to provide all the necessary information about the User object 
    let newUser = new User({
        firstName: reqBody.firstName,
        lastName: reqBody.lastName,
        email: reqBody.email,
        // 10 - is the number of salt rounds that bcrypt algorithm will run in order to encrypt the password.
        password: bcrypt.hashSync(reqBody.password, 10)
    })

    // Saves the created object to our database
    return newUser.save().then((user, error) => {
        if(error) {
            return false;
        } else {
            return true;
        }
    }).catch(err => err);
};

// Controllers to login a user
module.exports.loginUser = (req, res) => {
    return User.findOne({email: req.body.email}).then(result => { 

        if(result == null) {
            return res.send(false)
        } else {
            const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password);
            if(isPasswordCorrect) {
                return res.send({access: auth.createAccessToken(result)})
            } else {
                return res.send(false)
            }
        }
    }).catch(err => res.send(err));
};


// Route for retrieving authenticated user details
module.exports.getUserDetail = (req, res) => {
    return User.findById(req.user.id).then(result => {
            
            result.password = "";
            
            return res.send(result); 
            
        }).catch(err => res.send(err));
};

// Changing a Password
module.exports.resetPassword = async (req, res) => {
  try {
    const { newPassword } = req.body;
    const { id } = req.user; // Extracting user ID from the authorization header

    // Hashing the new password
    const hashedPassword = await bcrypt.hash(newPassword, 10);

    // Updating the user's password in the database
    await User.findByIdAndUpdate(id, { password: hashedPassword });

    // Sending a success response
    res.status(200).json(true);
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Internal server error' });
  }
};

// Updating a profile informatio
module.exports.updateProfile = async (req, res) => {
  try {
    // Get the user ID from the authenticated token
    const userId = req.user.id;

    // Retrieve the updated profile information from the request body
    const { firstName, lastName, mobileNo } = req.body;

    // Update the user's profile in the database
    const updatedUser = await User.findByIdAndUpdate(
      userId,
      { firstName, lastName, mobileNo },
      { new: true }
    );

    res.json(updatedUser);
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Failed to update profile' });
  }
};


// Update user admin status
module.exports.updateUserAdminStatus = async (req, res) => {
  const { userId } = req.body;

  try {
    // Find the user to update
    const user = await User.findById(userId);

    if (!user) {
      return res.status(404).json({ error: 'User not found.' });
    }

    // Update the admin status
    user.isAdmin = true;
    await user.save();

    res.json({ message: 'User admin status updated successfully.' });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Server error.' });
  }
};

