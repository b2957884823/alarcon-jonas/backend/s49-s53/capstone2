const Order = require("../models/Order");
const User = require("../models/User");
const Product = require("../models/Product");


// Controller for user to create order
module.exports.createOrder = async (req, res) => {
  try {
    
    const { productId, name, quantity } = req.body;
    const userId = req.user.id;

    const products = await Product.findById(productId);
    if (!products) {
      return res.send(false)
    }

    if(req.user.isAdmin) {
      return res.send(false)
    }

    const price = products.price;
    const totalAmount = price * quantity;

    // Create a new order
    const order = new Order({
      userId,
      products: [{ productId, name, quantity, price }],
      totalAmount
    });

    await order.save();

    return res.send(true)
  } catch (error) {
    console.error(error);
    return res.send(false)
  }
};


// Controller for getting all orders (ADMIN)
module.exports.getAllOrders = (req, res) => {

  Order.find({}).then((result, error) => {
    if(error) {
      return res.send(false)
    } else {
      return res.send(result)
    }
  })
  .catch(err => res.send(err))

};

module.exports.getUserOrders = async (req, res) => {
  try {
    const userId = req.user.id;

    // Assuming you have a "User" model to check if the user exists
    const userExists = await User.findById(userId);
    if (!userExists) {
      return res.status(404).json({ message: 'User not found.' });
    }

    // Fetch orders for the specific user
    const orders = await Order.find({ userId }).populate('products.productId', 'name');

    return res.json(orders);
  } catch (error) {
    console.error('Error fetching user orders:', error);
    return res.status(500).json({ message: 'An error occurred while fetching user orders.' });
  }
};
