const express = require('express');
const orderController = require('../controllers/order');
const auth = require('../auth');


const { verify, verifyAdmin } = auth;

const router = express.Router();







// [SECTION] Create a order POST - Create
router.post("/createOrder", verify, orderController.createOrder);





// Route for user to retrieve all orders
router.get("/getOrders", verify, orderController.getUserOrders);




// Route for retrieve all orders (ADMIN)
router.get("/getAllOrders", verify, verifyAdmin, orderController.getAllOrders);





module.exports = router;