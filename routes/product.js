const express = require('express');
const productController = require('../controllers/product');
const auth = require('../auth');


const { verify, verifyAdmin } = auth;

const router = express.Router();




// [SECTION] Create a product - Create 1
router.post("/addproduct", verify, verifyAdmin, productController.addProduct);


// [SECTION] Route for retrieving all the product {admin} 2
router.get("/all", productController.getAllProduct);


// [SECTION] Route for retrieving all the ACTIVE product for all the users. 3
router.get("/active", verify, productController.getAllActive);


// This function can search single product  4
router.get("/single/:productId", productController.getSingleProduct);



// [SECTION]  Route for updating a product (Admin)  5
// Only Admin will have access
router.put("/:productId", verify, verifyAdmin, productController.updateProduct);


// Route for Archiving Product  6 
router.put("/:productId/archive", verify, verifyAdmin, productController.archiveProduct);


// Route for Activate product  7  
router.put("/:productId/activate", verify, verifyAdmin, productController.activateProduct);


// Route to search for products by product name  8
router.post('/search', productController.searchProductsByName);



module.exports = router;